package init;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import init.Config;

import init.LogUtil;
import init.BrowserType;
import init.Initialization;

public interface BrowserController  {

	Initialization in = new Initialization();

	

	/**
	 * @brief Closes the page that driver is in control and shuts down the process
	 *        of driver.
	 */
	public default void closeBrowser() {
		LogUtil.logger.debug("Browser has been closed.");
		in.driver.quit();

	}

	public default void openBrowserHepsi(BrowserType browserType) {
		if (Config.gridEnable.equals("0")) {
			LogUtil.logger.debug(browserType + " has been initiated.");
			in.init(browserType);
			in.driver.get("https://www.hepsiburada.com/cozummerkezi");
			in.driver.manage().window().maximize();
//			in.driver.findElement(By.id("overridelink")).click();
			
		} else if (Config.gridEnable.equals("1")) {
			if (browserType == BrowserType.CHROME) {
				openBrowserHepsi("CHROME");
			} else if (browserType == BrowserType.FIREFOX) {
				openBrowserHepsi("FIREFOX");
			} else if (browserType == BrowserType.IE) {
				openBrowserHepsi("INTERNET EXPLORER");
			
				
			}
//			in.driver.get(Var.baseURLTams);
//			in.driver.manage().window().maximize();
//			in.driver.findElement(By.id("overridelink")).click();
			
		} else {
			org.testng.Assert.fail("gridEnable setting at config.XML has not been set.");
		}

	}
	
	public default void openBrowserHepsi(String browserType, String platform, String version, String nodeIP) {
		String browType = browserType.toUpperCase();
		if (Config.gridEnable.equals("0")) {
			openBrowserHepsi(BrowserType.valueOf(browserType.toUpperCase()));
			LogUtil.logger.info("Selenium Grid has been Disabled.");
		} else if (Config.gridEnable.equals("1")) {
			DesiredCapabilities capability = null;
			if (browType.equals("CHROME")) {
				capability = DesiredCapabilities.chrome();
				capability.setBrowserName("chrome");
				capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				
			} else if (browType.equals("INTERNET EXPLORER") || browType.equals("IE")) {
				capability = DesiredCapabilities.internetExplorer();
				capability.setBrowserName("internet explorer");
				capability.setCapability("ie.ensureCleanSession", true);
				capability.setCapability("nativeEvents", true);
				if(Config.acceptSSL.equals("1")){
					capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					in.driver.navigate ().to ("javascript:document.getElementById('overridelink').click()");
//					in.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//				    in.driver.findElement(By.xpath("//a[@id='overridelink']"));
//				    in.driver.findElement(By.id("overridelink"));
//				    JavascriptExecutor executor = (JavascriptExecutor)in.driver;
//				    executor.executeScript("arguments[0].click();", element);

				
					//waitAndClick("//a[@id='overridelink']");
				}
			} else if (browType.equals("FIREFOX")) {
				capability = DesiredCapabilities.firefox();
				capability.setBrowserName("firefox");
				capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			}

			if (capability != null) {
				capability.setPlatform(Platform.valueOf(platform));
				capability.setVersion(version);

				try {
					in.driver = new RemoteWebDriver(new URL(nodeIP), capability);
				} catch (MalformedURLException e) {
					LogUtil.logger.error(e.getMessage());
				}
			} else {
				org.testng.Assert.fail("Browser Capabilities has not been set.");
			}
			
			in.driver.get("https://www.hepsiburada.com/cozummerkezi");
			in.driver.manage().window().maximize();
		} else {
			org.testng.Assert.fail("gridEnable setting at config.XML has not been set.");
		}
		

	}
	
	public default void openBrowserHepsi(String browserType) {
		if (Config.gridEnable.equals("0")) {
			if (browserType.equals("internet explorer") || browserType.equals("INTERNET EXPLORER")) {
				openBrowserHepsi(BrowserType.IE);
			} else {
				openBrowserHepsi(BrowserType.valueOf(browserType.toUpperCase()));
			}
		} else if (Config.gridEnable.equals("1")) {
			openBrowserHepsi(browserType, Config.gridPlatform, Config.gridVersion, Config.gridNodeIP);
			LogUtil.logger.info("Selenium Grid has been Enabled.");
		} else {
			org.testng.Assert.fail("gridEnable setting at config.XML has not been set.");
		}
		
		
	}

	public default void openBrowserHepsi() {
		if (Config.gridEnable.equals("1")) {
			openBrowserHepsi(Config.gridBrowserType, Config.gridPlatform, Config.gridVersion, Config.gridNodeIP);
		} else if (Config.gridEnable.equals("0")) {
			openBrowserHepsi(Config.gridBrowserType);
		} else {
			org.testng.Assert.fail("gridEnable setting at config.XML has not been set.");
		}
	}
	

	



	

	/**
	 * @brief Changes HTML frames that is being worked on.
	 * @param frameId
	 *            uses HTML frame tag's ID.
	 */
	public default void changeFrame(String frameId) {
		in.driver.switchTo().frame(frameId);
	}

	/**
	 * @brief Changes HTML frames that is being worked on.
	 * @param frameId
	 *            uses HTML frame tag's ID.
	 */
	public default void changeFrame(int frameId) {
		in.driver.switchTo().frame(frameId);
	}

	/**
	 * @brief Changes the frame to top of the webpage which is default content.
	 */
	public default void changeFrameToDefaultContent() {
		in.driver.switchTo().defaultContent();
	}
	
	/**
	 * @brief Changes the frame to upper frame of the current frame.
	 */
	public default void changeFrameToParentFrame() {
		in.driver.switchTo().parentFrame();
	}

	/**
	 * @brief Refreshes the page that driver is in control.
	 */
	public default void refreshBrowser() {
		in.driver.navigate().refresh();
		LogUtil.logger.debug("Browser (" + in.driver.getWindowHandle() + ") has been initiated.");
	}

	/**
	 * @brief Goes to the previous page that driver is in control.
	 */
	public default void goToPreviousPage() {
		in.driver.navigate().back();
		LogUtil.logger.debug("Browser( " + in.driver.getWindowHandle() + ") has been gone to previous page.");
	}

	/**
	 * @brief Waits until the page is fully loaded.
	 */
	public default void waitForLoad() {

		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(in.driver, 30);
		wait.until(pageLoadCondition);
		LogUtil.logger.debug("Browser (" + in.driver.getWindowHandle() + ") has waited for the page to load.");

	}

	/**
	 * @brief Used for sleeping the thread. Do not use it unless it's a must to use.
	 * @param millisecond
	 */
	public default void sleep(long millisecond) {
		try {
			Thread.sleep(millisecond);
		} catch (Exception e) {
			LogUtil.logger.error("input format is wrong");
		}
	}

	/**
	 * @brief returns the title of the Browser which webdriver is on.
	 * 
	 */
	public default String getBrowserTitle() {
		String title = "";
		title = in.driver.getTitle();
		LogUtil.logger.info("Title of browser is : " + title);
		return title;
	}

	/**
	 * @brief fetches the string representing the Current URL
	 */
	public default String getCurrentURL() {
		String currentURL = in.driver.getCurrentUrl();
		LogUtil.logger.info("Current URL is : " + currentURL);
		return currentURL;

	}

	/**
	 * @brief fetches the string representing the Current URL
	 */
	public default String getPageSource() {
		String pageSource = in.driver.getPageSource();
		LogUtil.logger.info("Page source is  " + pageSource);
		return pageSource;
	}

	/**
	 * @brief Goes to the forward page that driver is in control.
	 */
	public default void goToForwardPage() {
		in.driver.navigate().forward();
		LogUtil.logger.debug("Browser( " + in.driver.getWindowHandle() + ") has been gone to forward page.");

	}

	/**
	 * @brief Minimizes the window.
	 */
	public default void minimizeWindow() {
		in.driver.manage().window().setSize(new Dimension(800, 621));
	}

	/**
	 * @brief Maximize the window with specific width and height.
	 */
	public default void setWindowSize(int width, int height) {
		in.driver.manage().window().setSize(new Dimension(width, height));
		LogUtil.logger.info("Window size is setting with width " + width + " and height " + height);
	}

	public default void deleteBrowserCookies() {
		in.driver.manage().deleteAllCookies();
	}
		
	
	
	
	
	/**
	
	*@brief Drag {@from} drop {@to}
	*/
	public default void dragAndDrop(String from,String to) {
		Actions act = new Actions(in.driver);
	WebElement from1=	in.driver.findElement(By.xpath(from));
	
	WebElement to1= in.driver.findElement(By.xpath(to));
	
	
	try {
		sleep(4000);
		Action dragndrop = act.clickAndHold(from1).moveToElement(to1).release(to1).build();
	} catch (Exception e) {
		
		System.out.println(e);
	}
		
	}
}