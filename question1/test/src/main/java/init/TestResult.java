package init;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;
import org.testng.Reporter;

import init.Config;
import init.LogUtil;

import io.qameta.allure.Step;
import junit.framework.ComparisonFailure;

public interface TestResult extends BrowserController {

	

	/**
	 * @brief Saves the trace and takes a screenshot automagically.
	 * @param error
	 * @deprecated (this method is not being used anymore and it will be removed soon.)
	 */
	@Deprecated
	public default void testFailure(Object error) {
		in.testResult = false;
		LogUtil.logger.info("----testFailure operation begins----");
	


		if (error instanceof AssertionError) {
			LogUtil.logger.error(((AssertionError) error).getMessage());
		} else if (error instanceof TimeoutException) {
			LogUtil.logger.error(((WebDriverException) error).getMessage());
		} else if (error instanceof ElementNotVisibleException) {
			LogUtil.logger.error(((ElementNotVisibleException) error).getMessage());
		} else if (error instanceof ElementNotSelectableException) {
			LogUtil.logger.error(((ElementNotSelectableException) error).getMessage());
		} else if (error instanceof StaleElementReferenceException) {
			LogUtil.logger.error(((StaleElementReferenceException) error).getMessage());
		} else if (error instanceof NoSuchElementException) {
			LogUtil.logger.error(((NoSuchElementException) error).getMessage());
		} else if (error instanceof NullPointerException) {
			LogUtil.logger.error(((NullPointerException) error).getStackTrace());
		} else if (error instanceof NoSuchWindowException) {
			LogUtil.logger.error(((NoSuchWindowException) error).getMessage());
		} else if (error instanceof AWTException) {
			LogUtil.logger.error(((AWTException) error).getMessage());
		} else if (error instanceof SQLException) {
			LogUtil.logger.error(((SQLException) error).getMessage());
		} else if (error instanceof ComparisonFailure) {
			LogUtil.logger.error(((ComparisonFailure) error).getMessage());
		} else if (error instanceof IOException) {
			LogUtil.logger.error(((IOException) error).getMessage());
		} else if (error instanceof InvalidSelectorException) {
			LogUtil.logger.error(((InvalidSelectorException) error).getMessage());
		} else if (error instanceof NoSuchFrameException) {
			LogUtil.logger.error(((NoSuchFrameException) error).getMessage());
		}

	
	}

	public static void testSuccess() {

	}

	/**
	 * @brief Takes info to logger. Meant to be used for comments.
	 * @param log
	 *            Needs to be taken in double quotes.
	 */
	@Step("{log}")
	public default void info(String log) {
		LogUtil.logger.info(log);
		Reporter.log(log);
	}

	@Deprecated
	public default void takeScreenShot(String filePath) {
		try {
			in.robot = new Robot();
			String fileName = filePath;
			String format = "jpg";

			Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
			BufferedImage screenFullImage = in.robot.createScreenCapture(screenRect);
			ImageIO.write(screenFullImage, format, new File(fileName));

			LogUtil.logger.debug("A full screenshot saved!");
		} catch (AWTException | IOException ex) {
			testFailure(ex);
		}
	}

	/**
	 * @brief Checks if the WebElement given is found multiple times which is
	 *        incorrect.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default void checkWebElementCount(By by) {
		try {
			if (in.driver.findElements(by).size() > 1) {
				Assert.fail("There are more than one WebElement of " + by.toString() + " . There has to be one.");
				
			}
		} catch (InvalidSelectorException e) {
			Assert.fail(e.getMessage());
		}
	}
}
