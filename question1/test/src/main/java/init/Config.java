package init;



public class Config   {

    private Config() {

	}

	public static final String firefoxDriver = XmlFileParse.xmlFileParse("firefoxDriver", "config.xml");
	public static final String chromeDriver = XmlFileParse.xmlFileParse("chromeDriver", "config.xml");
	public static final String ieDriver = XmlFileParse.xmlFileParse("ieDriver", "config.xml");
	public static String acceptSSL = XmlFileParse.xmlFileParse("acceptSSL", "config.xml");
	public static String gridEnable = XmlFileParse.xmlFileParse("gridEnable", "config.xml");
	public static String gridBrowserType = XmlFileParse.xmlFileParse("gridBrowserType", "config.xml");
	public static String gridPlatform = XmlFileParse.xmlFileParse("gridPlatform", "config.xml");
	public static String gridVersion = XmlFileParse.xmlFileParse("gridVersion", "config.xml");
	public static String gridNodeIP = XmlFileParse.xmlFileParse("gridNodeIP", "config.xml");

	public static final int wait = 30;
	public static final int waitBeforeTest = 300;
}
