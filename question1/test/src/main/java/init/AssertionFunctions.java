package init;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import init.Config;

public interface AssertionFunctions extends TestResult, NavigationController, BrowserController {

	/**
	 * @brief Waits for the WebElement which will appear.
	 * @param xPATH
	 *            Needs xPath in double quotes.
	 */
	
	public default void waitForElementAppeared(String xPATH) {
		waitForElementAppeared(By.xpath(xPATH));
	}

	/**
	 * @brief Waits for the WebElement which will appear.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default void waitForElementAppeared(By by) {
		checkWebElementCount(by);
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	/**
	 * @brief Waits for the WebElement which has disappeared.
	 * @param xPATH
	 *            Needs xPath in double quotes.
	 */
	public default void waitForElementDisappeared(String xPATH) {
		waitForElementDisappeared(By.xpath(xPATH));
	}

	/**
	 * @brief Waits for the WebElement which has disappeared.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default void waitForElementDisappeared(By by) {
		checkWebElementCount(by);
		waitForElementAppeared(by);
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	/**
	 * @brief Waits until the text is present.
	 * @param xPATH
	 *            Needs xPath in double quotes.
	 * @param text
	 *            Needs xPath in double quotes.
	 */
	public default void textToBePresentInElementLocated(String xPATH, String text) {
		textToBePresentInElementLocated(By.xpath(xPATH), text);
	}

	/**
	 * @brief Waits until the text is present.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 * @param text
	 *            Needs xPath in double quotes.
	 */
	public default void textToBePresentInElementLocated(By by, String text) {
		checkWebElementCount(by);
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(by, text));
		assertEquals(getText(by), text);
	}

	/**
	 * @brief Makes an assertion if the expected attribute's value is equal to
	 *        actual attribute's value
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username") * @param attributeName
	 * @param expectedAttribute
	 *            written inside double quotes.
	 * @param attributeName written inside double quotes.
	 */
	public default void assertAttribute(By by, String attributeName, String expectedAttribute) {
		assertEquals(getElementAttribute(by, attributeName), expectedAttribute);
	}

	/**
	 * @brief Makes an assertion if the expected attribute's value is equal to
	 *        actual attribute's value
	 * @param xPATH Needs xPath in double quotes.
	 * @param expectedAttribute
	 *            written inside double quotes.
	 * @param attributeName written inside double quotes.
	 */
	public default void assertAttribute(String xPATH, String attributeName, String expectedAttribute) {
		assertAttribute(By.xpath(xPATH), attributeName, expectedAttribute);
	}
	
	/**
	 * @brief Gets text of a Web Element and makes an assertion if the text that comes from webElement is equal to the expected result.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username") 
	 * @param expected written inside double quotes.
	 */
	public default void assertTextOfWebElement(By by, String expected) {
		assertEquals(getText(by), expected);
	}
	
	/**
	 * @brief Gets text of a Web Element and makes an assertion if the text that comes from webElement is equal to the expected result.
	 * @param xPATH Needs xPath in double quotes.
	 * @param expected written inside double quotes.
	 */
	public default void assertTextOfWebElement(String xPATH, String expected) {
		assertTextOfWebElement(By.xpath(xPATH), expected);
	}
	
	/**
	 * @Brief Checks if the Web Element is clickable.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username") 
	 * @return returns true if clickable.
	 */
	public default boolean isElementClickable(By by) {
		return in.driver.findElement(by).isEnabled();
	}
	
	/**
	 * @Brief Checks if the Web Element is clickable.
	 * @param xPATH Needs xPath in double quotes.
	 * @return returns true if clickable.
	 */
	public default boolean isElementClickable(String xPATH) {
		return isElementClickable(By.xpath(xPATH));
	}
	

	

}
