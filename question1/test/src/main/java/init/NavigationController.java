package init;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.awt.GraphicsEnvironment;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import init.Config;
import init.LogUtil;
import init.BrowserType;


/**
 * @brief Navigation Controller functions. Powered by Selenium.
 * @author bg
 *
 */
public interface NavigationController extends TestResult, BrowserController {

	/**
	 * @brief Waits until the WebElement is located in the page and it is clickable,
	 *        then clicks to the element.
	 * @param xPATH
	 *            Needs xPath in double quotes.
	 */
	public default void waitAndClick(String xPATH) {
		waitAndClick(By.xpath(xPATH));
	}
	
	

	/**
	 * @brief Waits until the WebElement is located in the page and it is clickable,
	 *        then clicks to the element.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default void waitAndClick(By by) {
		checkWebElementCount(by);
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		wait.until(ExpectedConditions.elementToBeClickable(by));
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		in.driver.findElement(by).click();

	}

	/**
	 * @brief Finds the WebElement by a text which element contains and clicks to
	 *        it. Does not work if there are whitespaces inside the webelement's
	 *        text.
	 * @param text
	 *            Takes the text of the WebElement.
	 */
	public default void containsTextClick(String text) {

		// Wait until upload button is visible
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(), '" + text + "')]")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(), '" + text + "')]")));
		in.driver.findElement(By.xpath("//*[contains(text(), '" + text + "')]")).click();
	}

	/**
	 * @brief Finds the WebElement by it's starting text and clicks to it.
	 * @param text
	 *            Takes the text of the WebElement.
	 */
	public default void startsWithTextClick(String text) {
		// Wait until upload button is enabled and clickable
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[starts-with(text(), '" + text + "')]")));
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[starts-with(text(), '" + text + "')]")));
		in.driver.findElement(By.xpath("//*[starts-with(text(), '" + text + "')]")).click();
	}

	/**
	 * @brief Finds the WebElement by it's text and clicks to it.
	 * @param text
	 *            Takes the text of the WebElement.
	 */
	public default void findByTextClick(String text) {
		// Wait until upload button is enabled and clickable
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='" + text + "']")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='" + text + "']")));
		in.driver.findElement(By.xpath("//*[text()='" + text + "']")).click();
	}

	/**
	 * @brief Waits until the WebElement is located and visible then enters the
	 *        input.
	 * @param xPATH
	 *            Needs xPath in double quotes.
	 * @param input
	 *            Takes the input. Needs to be written in double quotes.
	 */
	public default void waitAndSendKeys(String xPATH, String input) {
		waitAndSendKeys(By.xpath(xPATH), input);
	}

	/**
	 * @brief Waits until the WebElement is located and visible then enters the
	 *        input.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username") * @param input
	 */
	public default void waitAndSendKeys(By by, String input) {
		checkWebElementCount(by);
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		By webElem = by;
		// Wait until element is visible
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		wait.until(ExpectedConditions.presenceOfElementLocated(webElem));
		in.driver.findElement(webElem).sendKeys(input);
	}

	/**
	 * @brief Clear input text area.
	 * @param xPATH
	 *            Needs xPath in double quotes.
	 */
	public default void clearInputTextArea(String xPATH) {
		in.driver.findElement(By.xpath(xPATH)).sendKeys(Keys.BACK_SPACE);
		in.driver.findElement(By.xpath(xPATH)).sendKeys(Keys.BACK_SPACE);
		clearInputTextArea(By.xpath(xPATH));
	}

	/**
	 * @brief Clear input text area.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default void clearInputTextArea(By by) {
		in.driver.findElement(by).clear();
	}

	/**
	 * @brief Used for pressing keys of the keyboard. Cannot be used out of
	 *        WebDriver's jurisdiction such as file dialogs.
	 * @param xPATH
	 *            Needs xPath in double quotes.
	 * @param charSeq
	 *            Needs to be used starting with 'Keys.'. for example for pressing
	 *            Down button: 'Keys.DOWN'
	 */
	public default void keyboardAction(String xPATH, CharSequence charSeq) {
		in.driver.findElement(By.xpath(xPATH)).sendKeys(charSeq);
	}

	/**
	 * @brief Switches to the pop up and accepts it. Then switches back to the
	 *        active page.
	 */
	public default void acceptWindowAlert() {
		in.driver.switchTo().alert().accept();
		in.driver.switchTo().activeElement();
	}

	/**
	 * @brief Switches to the pop up and dismisses it. Then switches back to the
	 *        page that were being used by WebDriver again.
	 */
	public default void dismissWindowAlert() {
		String windowHandle = in.driver.getWindowHandle();
		in.driver.switchTo().alert().dismiss();
		in.driver.switchTo().window(windowHandle);
	}

	/**
	 * @brief Switches to the window by it's title.
	 * @param title
	 *            Needs to be written in double quotes.
	 */
	public default void selectWindow(String title) {
		for (String winHandle : in.driver.getWindowHandles()) {
			if (in.driver.switchTo().window(winHandle).getTitle().equals(title)) {
				return;
			}
		}
		Assert.fail("Window with title: " + title + "cannot be found.");
	}

	/**
	 * @brief Returns the current window handle as a String for future use.
	 * @return
	 */
	public default String getWindowHandle() {
		return in.driver.getWindowHandle();
	}

	/**
	 * @brief Switches to a different window depending on the window handle.
	 *        getWindowHandle() can be used here.
	 * @param windowHandle
	 *            getWindowHandle() can be used here.
	 */
	public default void switchToWindow(String windowHandle) {
		in.driver.switchTo().window(windowHandle);
	}

	/**
	 * @brief Switches to the child window itself. To return back, it is recommended
	 *        to hold the main window handle within a String value with
	 *        getWindowHandle(), then switch window with it.
	 */
	public default void switchToChildWindow() {

		for (String winHandle : in.driver.getWindowHandles()) {
			in.driver.switchTo().window(winHandle);
		}

	}
	/*
	 * // public default void blockPopUpsWIP() { // if (in.BrowType == "Chrome") {
	 * // ChromeOptions options = new ChromeOptions(); //
	 * options.addArguments("--disable-popup-blocking"); //
	 * options.addArguments("chrome.switches","--disable-extensions"); // in.driver
	 * = new ChromeDriver(options); // } // else if (in.BrowType == "Firefox") { //
	 * // } // else if (in.BrowType == "IE") { // // } // }
	 */

	/**
	 * @brief Gets text of the WebElement then returns it as a String.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default String getText(By by) {
		checkWebElementCount(by);
		String text = "";

		// Wait until element is visible
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		text = in.driver.findElement(by).getText();

		return text;
	}

	/**
	 * @brief Gets text of a WebElement.
	 * @param xPATH
	 *            Needs to be written in double quotes.
	 * @return
	 */
	public default String getText(String xPATH) {
		return getText(By.xpath(xPATH));
	}
	
	/**
	 * @brief Gets value of the Input WebElement then returns it as a String.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default String getInputValue(By by) {
		checkWebElementCount(by);
		String text = "";

		// Wait until element is visible
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		text = in.driver.findElement(by).getAttribute("value");

		return text;
	}

	/**
	 * @brief Gets value of an Input WebElement.
	 * @param xPATH
	 *            Needs to be written in double quotes.
	 * @return
	 */
	public default String getInputValue(String xPATH) {
		return getInputValue(By.xpath(xPATH));
	}
	
	

	/**
	 * @brief Hovers cursor to the WebElement.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default void hoverTo(By by) {
		checkWebElementCount(by);
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		// Wait until element is visible
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));

		in.actions = new Actions(in.driver);
		WebElement webelem = in.driver.findElement(by);
		in.actions.moveToElement(webelem).build().perform();

	}

	/**
	 * @brief Hovers cursor to the WebElement.
	 * @param xPATH
	 *            Needs to be written in double quotes.
	 */
	public default void hoverTo(String xPATH) {
		hoverTo(By.xpath(xPATH));
	}

	/**
	 * @brief Hovers cursor to the WebElement and waits given time .
	 * @param by
	 *            time
	 */
	public default void hoverToWithTime(By by, long time) {
		checkWebElementCount(by);
		WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
		// Wait until element is visible
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));

		in.actions = new Actions(in.driver);
		WebElement webelem = in.driver.findElement(by);
		in.actions.moveToElement(webelem);
		in.actions.build().perform();
		LogUtil.logger.info("Succesfully performed hover action");
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			LogUtil.logger.error(e.getMessage());
		}
		LogUtil.logger.info("It is waiting for given time");

	}

	/**
	 * @brief Used for right clicking on an element. To be able to click to a
	 *        selection inside context, you need to use keyboardAction(String,
	 *        CharSequence) for pressing Keys.DOWN and Keys.ENTER .
	 * @param xPATH
	 *            Needs to be written in double quotes.
	 */
	public default void rightClick(String xPATH) {
		rightClick(By.xpath(xPATH));
	}

	/**
	 * @brief Used for right clicking on an element. To be able to click to a
	 *        selection inside context, you need to use keyboardAction(String,
	 *        CharSequence) for pressing Keys.DOWN and Keys.ENTER .
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default void rightClick(By by) {
		checkWebElementCount(by);
		in.actions = new Actions(in.driver).contextClick(in.driver.findElement(by));
		in.actions.build().perform();

		LogUtil.logger.error("Sucessfully Right clicked on the element");
	}

	/**
	 * @brief Used for double clicking on a WebElement.
	 * @param xPATH
	 *            Needs to be written in double quotes.
	 */
	public default void doubleClick(String xPATH) {
		doubleClick(By.xpath(xPATH));
	}

	/**
	 * @brief Used for double clicking on a WebElement.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default void doubleClick(By by) {
		checkWebElementCount(by);
		(new Actions(in.driver)).doubleClick(in.driver.findElement(by)).build().perform();
	}

	/**
	 * @brief Receives an attribute of WebElement as a String.
	 * @param xPATH
	 *            Needs to be written in double quotes.
	 * @param attribute
	 *            Needs to be written in double quotes.
	 */
	public default String getElementAttribute(String xPATH, String attribute) {
		return getElementAttribute(By.xpath(xPATH), attribute);
	}

	/**
	 * @brief Receives an attribute of WebElement as a String.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 * @param attribute
	 *            Needs to be written in double quotes.
	 */
	public default String getElementAttribute(By by, String attribute) {
		checkWebElementCount(by);
		return in.driver.findElement(by).getAttribute(attribute);
	}

	/**
	 * @brief Sets an element's attribute to the desired value.
	 * @param xPATH
	 *            Needs to be written in double quotes.
	 * @param attributeName
	 *            Needs to be written in double quotes.
	 * @param attributeValue
	 *            Needs to be written in double quotes.
	 */
	public default void setElementAttribute(String xPATH, String attributeName, String attributeValue) {
		setElementAttribute(By.xpath(xPATH), attributeName, attributeValue);
	}

	/**
	 * @brief Sets an element's attribute to the desired value.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 * @param attributeName
	 *            Needs to be written in double quotes.
	 * @param attributeValue
	 *            Needs to be written in double quotes.
	 */
	public default void setElementAttribute(By by, String attributeName, String attributeValue) {

		WebElement element = in.driver.findElement(by);
		JavascriptExecutor js = (JavascriptExecutor) in.driver;
		js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);", element, attributeName,
				attributeValue);

	}

	/**
	 * @brief Makes the browser go to the link.
	 * @param url
	 *            Full URL needs to be written in double quotes.
	 */
	public default void getUrl(String url) {
		in.driver.get(url);
		waitForLoad();
	}

	/**
	 * @brief Selects an item from drop down by the option's value or text.
	 * @param xPATH
	 * @param optionValue
	 */
	public default void selectItemFromDropDown(String xPATH, String optionValue) {
		Select dropdown = new Select(in.driver.findElement(By.xpath(xPATH)));
		dropdown.selectByVisibleText(optionValue);
	}

	/**
	 * @brief Handles download dialog of Firefox and IE. Does not support Chrome
	 *        because Chrome does not need it.
	 */
	public default void handleDownloadDialog() {

		try {
			in.robot = new Robot();
		} catch (AWTException e) {
			LogUtil.logger.error(e.getStackTrace());
		}
		if (in.browType == BrowserType.FIREFOX) {
			in.robot.keyPress(KeyEvent.VK_DOWN);
			in.robot.keyRelease(KeyEvent.VK_DOWN);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				LogUtil.logger.error(e.getStackTrace());
			}
			in.robot.keyPress(KeyEvent.VK_ENTER);
			in.robot.keyRelease(KeyEvent.VK_ENTER);
		} else if (in.browType == BrowserType.IE) {
			in.robot.keyPress(KeyEvent.VK_LEFT);
			in.robot.keyRelease(KeyEvent.VK_LEFT);
			in.robot.keyPress(KeyEvent.VK_ENTER);
			in.robot.keyRelease(KeyEvent.VK_ENTER);
			in.robot.keyPress(KeyEvent.VK_ENTER);
			in.robot.keyRelease(KeyEvent.VK_ENTER);
		} else if (in.browType == BrowserType.CHROME) {
			return;
		}

	}

	/**
	 * @brief Scrolls until WebElement is on the screen.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 */
	public default void scrollToWebElement(By by) {
		checkWebElementCount(by);
		WebElement element = in.driver.findElement(by);
		((JavascriptExecutor) in.driver).executeScript("arguments[0].scrollIntoView(true);", element);
		sleep(500);
	}

	/**
	 * @brief Scrolls until WebElement is on the screen.
	 * @param xPATH
	 *            Needs to be written in double quotes.
	 */
	public default void scrollToWebElement(String xPATH) {
		scrollToWebElement(By.xpath(xPATH));
	}

	/**
	 * @brief Presses a keyboard key
	 * @param key
	 *            keyboard action name sample usage: pressKey(KeyEvent.VK_ENTER)
	 */
	public default void pressKey(int key) {
		try {
			in.robot = new Robot();
		} catch (AWTException e) {
			LogUtil.logger.error(e.getStackTrace());
		}
		in.robot.keyPress(key);
		in.robot.keyRelease(key);
	}

	/**
	 * @brief Moves the cursor to specified WebElement's location.
	 * @param by
	 *            By class is a mechanism used to locate elements within a document.
	 *            eg. By.id("username")
	 * @throws AWTException
	 */
	public default void hoverCursor(By by) throws AWTException {
		int usefulScreenHeight = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height;

		WebElement browserSize = in.driver.findElement(By.xpath("/html"));
		org.openqa.selenium.Dimension browserSizeHW = browserSize.getSize();

		int browserHeight = browserSizeHW.getHeight();
		int carryheight = usefulScreenHeight - browserHeight;
		// Locate element for which you wants to retrieve x y coordinates.
		WebElement image = in.driver.findElement(by);
		// Used points class to get x and y coordinates of element.
		Point location = image.getLocation();
		int xbro = location.getX();
		int ybro = location.getY() + carryheight;
		Robot robot = new Robot();
		robot.mouseMove(xbro, ybro);
		robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);

	}

	/**
	 * @brief Moves the cursor to specified WebElement's location.
	 * @param xPATH
	 *            Needs xPath in double quotes.
	 * @throws AWTException
	 */
	public default void hoverCursor(String xPATH) throws AWTException {
		hoverCursor(By.xpath(xPATH));
	}

	/**
	 * @brief Used for click on link which is between the 'a' tag in
	 * @param String
	 *            linkTxt Needs to be written in double quotes. e.g
	 */

	public default void clickOnLink(String linkTxt) {
		in.driver.findElement(By.linkText(linkTxt)).click();
		String opennedLink = in.driver.findElement(By.linkText(linkTxt)).getText();
		assertEquals(linkTxt, opennedLink, "Wrong link text is openned pls check code");
		LogUtil.logger.info("Sucessfully  clicked on the element URL");
	}

	/**
	 * @brief Scrolls down in 250 pixel range
	 * @param
	 */
	public default void scrollDown() {
		waitForLoad();
		JavascriptExecutor js = (JavascriptExecutor) in.driver;
		js.executeScript("window.scrollBy(0,250)", "");
		LogUtil.logger.info("Scroll down is happened with 250 pixel");
	}

	/**
	 * @brief Scrolls up in 250 pixel range
	 * @param
	 */
	public default void scrollUp() {
		waitForLoad();
		JavascriptExecutor js = (JavascriptExecutor) in.driver;
		js.executeScript("window.scrollBy(0,-250)", "");
		LogUtil.logger.info("Scroll up is happened with 250 pixel");
	}

    /**
     * @brief Clicks to Web Element with JavaScript Executor.
     * @param by
     *            By class is a mechanism used to locate elements within a document.
            *            eg. By.id("username")
            */
    public default void waitAndClickDirectJS(By by) {
        checkWebElementCount(by);
        WebDriverWait wait = new WebDriverWait(in.driver, Config.wait);
        wait.until(ExpectedConditions.elementToBeClickable(by));
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        WebElement ele = in.driver.findElement(by);
        JavascriptExecutor executor = (JavascriptExecutor)in.driver;
        executor.executeScript("arguments[0].click();", ele);

    }
    
    /**
     * @brief Scroll to Web Element with JavaScript Executor.
     * @param by
     *            By class is a mechanism used to locate elements within a document.
            *            eg. By.Xpath("xpath")
            */
    
public default void waitAndScrollInDropdown(String xpath) throws InterruptedException {
		
		JavascriptExecutor je = (JavascriptExecutor) in.driver;
        WebElement element = in.driver.findElement(By.xpath(xpath));
        je.executeScript("arguments[0].scrollIntoView(true);",element);
        Thread.sleep(2000);
		
	}
    

    /**
     * @brief Clicks to Web Element with JavaScript Executor.
     * @param xPATH
     *            Needs xPath in double quotes.
     */
    public default void waitAndClickDirectJS(String xPATH) {
        waitAndClickDirectJS(By.xpath(xPATH));
    }

}
